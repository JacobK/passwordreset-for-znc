# SPDX-FileCopyrightText: 2023 Jacob K
#
# SPDX-License-Identifier: Apache-2.0

passwordreset.so: passwordreset.cpp
	LIBS="sqlite3" znc-buildmod passwordreset.cpp

install: passwordreset.so
	cp passwordreset.so ~/.znc/modules
	cp -r passwordreset ~/.znc/modules

clean:
	rm passwordreset.so
