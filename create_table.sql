-- SPDX-FileCopyrightText: 2023 Jacob K
--
-- SPDX-License-Identifier: CC0-1.0

-- For information on data types in sqlite3, see https://www.sqlite.org/datatype3.html
-- I made the mistake of thinking I knew what I was doing since I used MySQL in the past...
create table verifications(username text primary key NOT NULL, code text NOT NULL, date_sent integer DEFAULT (strftime('%s','now')), used int DEFAULT FALSE);
insert into verifications(username, code) values("jacobk", 1234); -- This is what the SQL run by botnow will look like; don't forget to securely randomize the code!
-- select code from verifications where username is "jacobk";
