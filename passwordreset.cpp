// SPDX-FileCopyrightText: 2023 Jacob K
//
// SPDX-License-Identifier: Apache-2.0

#include <znc/main.h>
#include <znc/Modules.h>
#include <znc/User.h>
#include <znc/IRCNetwork.h>
#include <fstream>
#include <sqlite3.h>
#include <iostream>

const int PASSWORDRESET_CODE_TIMEOUT = 60 * 60 * 2; // 60 seconds in a minute * 60 minutes in an hour * 2 hours

class CPasswordResetMod : public CModule {
	public:
		MODCONSTRUCTOR(CPasswordResetMod) {
			sqlite3_open("example.db", &DB);
		}
		
		bool WebRequiresLogin() override {
			return false;
		}
		
		bool OnWebRequest(CWebSock& WebSock, const CString& sPageName,
				CTemplate& Tmpl) override {
				
			if (sPageName == "reset") {
				return handleFormSubmission(WebSock, sPageName, Tmpl);
			} else {
				return showResetForm(WebSock, sPageName, Tmpl);
			}
		}
	
	private:
		
		sqlite3* DB;
		
		// Sets the password of a user, assuming the change is already authorized
		// Returns false if the user doesn't exist
		bool setUserPassword(CUser* pUser, CString& password) {
			if (pUser == nullptr) {
				return false;
			}
			CString sSalt = CUtils::GetSalt();
			CString sHash = CUser::SaltedHash(password, sSalt);
			pUser->SetPass(sHash, CUser::HASH_DEFAULT, sSalt);
			return true;
		}
		
		bool handleFormSubmission(CWebSock& WebSock, const CString& sPageName,
				CTemplate& Tmpl) {
			CString oneTimeCode = WebSock.GetParam("onetimecode");
			CString sUsername = WebSock.GetParam("user");
			CUser* pUser = CZNC::Get().FindUser(sUsername);
			if (pUser == nullptr) {
				WebSock.PrintErrorPage(t_s("Invalid Submission [Outdated or broken link]"));
				return true;
			}
			CString password = WebSock.GetParam("password");
			CString password2 = WebSock.GetParam("password2");
			if (password != password2) {
				WebSock.PrintErrorPage(t_s("Invalid Submission [Passwords do not match]"));
				return true;
			}
			if (!checkCode(sUsername, oneTimeCode)) { // if code is invalid or outdated or already used
				WebSock.PrintErrorPage(t_s("Invalid Submission [Outdated or broken link]"));
				return true;
			}
			const bool resetSuccess = setUserPassword(pUser, password);
			if (!resetSuccess) {
				WebSock.PrintErrorPage(t_s("Unknown error"));
				return true;
			}
			const bool codeUsed = useCode(sUsername, oneTimeCode);
			if (!codeUsed) {
				std::cout << "ERROR: User " << sUsername << " reset their password but the verification code database entry did not properly update to indicate they used it!" << std::endl;
				WebSock.PrintErrorPage(t_s("Unknown error"));
				return true;
			}
			return true;
		}
		
		// The data type of outputValue must be a CString pointer! Requires that there only one data entry retrieved
		static int getSqlStringCallback(void* outputValue, int count, char** data, char** columns) {
			CString &s = *(static_cast<CString*>(outputValue)); // Thanks to Seb Holzapfel's StackOverflow answer at <https://stackoverflow.com/a/14553806>
			if (count == 1) {
//				std::cout << "s: " << s << std::endl;
//				std::cout << "data[0]: " << data[0] << std::endl; // for debugging
//				std::cout << "outputValue before setting: " << outputValue << std::endl; // for debugging
				s = data[0];
//				std::cout << "s after setting: " << s << std::endl; // for debugging
				return 0;
			} else {
				return 1;
			}
		}
		
		CString getSqlString(CString sqlQuery) {
			CString returnString = "null";
			sqlite3_exec(DB, sqlQuery.c_str(), getSqlStringCallback, &returnString, NULL); // .c_str() converts from CString (which is based on std::string) to const char*
			return returnString;
		}
		
		bool checkCode(CString user, CString code) {
			if (code == "null") {
				return false;
			}
			CString correctCode = getSqlString("select code from verifications where username is '" + user + "';"); // `'` and `"` are not valid username characters in znc
			if (correctCode != code) {
//				std::cout << "incorrect code of " << code << " instead of correct code " << correctCode << std::endl;
				return false;
			}
			std::time_t codeTimestamp = stoi(getSqlString("select date_sent from verifications where username is '" + user + "';"));
			std::time_t currentTimestamp = std::time(nullptr);
			std::time_t timeDifference = currentTimestamp - codeTimestamp;
			if (timeDifference > PASSWORDRESET_CODE_TIMEOUT) {
				std::cout << "User " << user << " tried to use an expired verification link. The verification link was generated at " << codeTimestamp << " but the current time is " << currentTimestamp << ". The maximum allowed difference is " << PASSWORDRESET_CODE_TIMEOUT << ", but the difference here is " << timeDifference << "." << std::endl;
				return false;
			}
//			std::cout << "code timestamp: " << codeTimestamp << std::endl;
//			std::cout << "current timestamp: " << std::time(nullptr) << std::endl;
			
			int used = stoi(getSqlString("select used from verifications where username is '" + user + "';"));
			if (used == 1) {
				return false;
			}
			return true;
		}
		
		bool useCode(CString user, CString code) {
			if (checkCode(user, code)) {
				sqlite3_exec(DB, "update verifications set used = TRUE where username is 'jacobk';", NULL, NULL, NULL);
				return true;
			} else {
				return false;
			}
		}
		
		bool showResetForm(CWebSock& WebSock, const CString& sPageName,
				CTemplate& Tmpl) {
			CString username = WebSock.GetParam("user", false);
			Tmpl["Username"] =  username; // Took me way too long to figure out I needed false here (false is for GET requests, true/default is for POST requests.).
			CString oneTimeCode = WebSock.GetParam("code", false);
			Tmpl["OneTimeCode"] = oneTimeCode;
			if (!checkCode(username, oneTimeCode)) {
				WebSock.PrintErrorPage(t_s("Invalid Page [Outdated or broken link]"));
				return true;
			}
			return true;
		}
};

GLOBALMODULEDEFS(
	CPasswordResetMod,
	t_s("Allows users to reset their password with a one-time link."))
